[![pipeline status](https://gitlab.com/quadrixo/libraries/php/web-application/badges/main/pipeline.svg)](https://gitlab.com/quadrixo/libraries/php/web-application/-/commits/main)  [![coverage report](https://gitlab.com/quadrixo/libraries/php/web-application/badges/main/coverage.svg)](https://gitlab.com/quadrixo/libraries/php/web-application/-/commits/main)

# quadrixo/web-application

`quadrixo/web-application` is a micro-framework based on dependency injection and compatible with PHP-FIG recommendations.

## License

This software is licensed under the [CeCILL](https://cecill.info/) license.

[CeCILL-2.1](./LICENSE)
