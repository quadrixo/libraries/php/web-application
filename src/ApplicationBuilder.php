<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/web-application library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\WebApp;

use Nyholm\Psr7Server\ServerRequestCreatorInterface;
use Psr\Http\Message\ServerRequestInterface;
use Quadrixo\Application;
use Quadrixo\ContainerAwareTrait;
use Quadrixo\WebApp\Internal\PipelineBuilderTrait;
use Quadrixo\WebApp\Internal\RequestHandlerWrapperTrait;
use Quadrixo\WebApp\Internal\ResponseSender;
use Quadrixo\WebApp\Traits\MiddlewaresTrait;

abstract class ApplicationBuilder extends \Quadrixo\ApplicationBuilder
{
    use ContainerAwareTrait;
    use MiddlewaresTrait;
    use PipelineBuilderTrait
    {
        build as buildPipeline;
    }

    private $pipelineBuilder;

    public function build(): Application
    {
        $pipeline = $this->buildPipeline();

        $application = new class() extends Application
        {
            use ContainerAwareTrait;
            use RequestHandlerWrapperTrait;

            public function run(ServerRequestInterface $request = null): void
            {
                if (is_null($request))
                {
                    $request = $this->getContainer()
                        ->get(ServerRequestCreatorInterface::class)
                        ->fromGlobals();
                }
                $response = $this->handle($request);

                $sender = new class() extends ResponseSender {};
                $sender->send($response);
            }
        };

        return $application
            ->setContainer($this->getContainer())
            ->wrap($pipeline);
    }
}
