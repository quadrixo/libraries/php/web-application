<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/web-application library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\WebApp\Traits;

use Closure;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Quadrixo\Middlewares\CorsMiddleware;
use Quadrixo\Middlewares\HstsMiddleware;
use Quadrixo\Middlewares\HttpsRedirectionMiddleware;
use Quadrixo\Middlewares\PlatesPageMiddleware;
use Quadrixo\Middlewares\StaticFilesMiddleware;
use Quadrixo\Middlewares\WebApi\WebApiRouter;
use Quadrixo\Middlewares\WebApiMiddleware;
use Quadrixo\WebApp\ApplicationBuilder;

trait MiddlewaresTrait
{
    /**
     * Adds middleware handling CORS requests.
     *
     * @return ApplicationBuilder
     */
    public function useCors(): ApplicationBuilder
    {
        $this->use(CorsMiddleware::class);
        return $this;
    }

    /**
     * Adds middleware for using HSTS, which adds the Strict-Transport-Security header.
     *
     * @return ApplicationBuilder
     */
    public function useHsts(): ApplicationBuilder
    {
        $this->use(HstsMiddleware::class);
        return $this;
    }

    /**
     * Adds middleware for redirecting HTTP Requests to HTTPS.
     *
     * @return ApplicationBuilder
     */
    public function useHttpsRedirection(): ApplicationBuilder
    {
        $this->use(HttpsRedirectionMiddleware::class);
        return $this;
    }

    /**
     * Enables static file serving for the current request path.
     *
     * @return ApplicationBuilder
     */
    public function useStaticFiles(): ApplicationBuilder
    {
        $this->use(StaticFilesMiddleware::class);
        return $this;
    }

    /**
     * Enables serving Plates page.
     *
     * @return ApplicationBuilder
     */
    public function usePlatesPage(): ApplicationBuilder
    {
        $this->use(PlatesPageMiddleware::class);
        return $this;
    }

    /**
     * Enables routing and handling the current path.
     *
     * @param Closure $config(WebApiRouter $router)
     * @return ApplicationBuilder
     */
    public function useWebApi(Closure $config): ApplicationBuilder
    {
        $this->use(function() use($config)
        {
            $router = new WebApiRouter($this);
            $config->call($this, $router);
            return new WebApiMiddleware(
                $router,
                $this,
                $this->get(ResponseFactoryInterface::class),
                $this->get(StreamFactoryInterface::class));
        });
        return $this;
    }
}
