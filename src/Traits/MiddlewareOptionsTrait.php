<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/web-application library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\WebApp\Traits;

use Closure;
use Quadrixo\Middlewares\CorsOptions;
use Quadrixo\Middlewares\HstsOptions;
use Quadrixo\Middlewares\HttpsRedirectionOptions;
use Quadrixo\Middlewares\PlatesEngineOptions;
use Quadrixo\Middlewares\PlatesPageOptions;
use Quadrixo\Middlewares\StaticFilesOptions;
use Quadrixo\WebApp\ContainerBuilder;

trait MiddlewareOptionsTrait
{
    private function addOptions(string $classname, ?Closure $config = null): void
    {
        $this->add($classname, $classname);
        if ($config !== null)
        {
            $this->configure($classname, $config);
        }
    }

    /**
     * Add CORS Services.
     *
     * @param Closure $config(CorsOptions $options)
     * @return ContainerBuilder
     */
    public function addCors(Closure $config = null): ContainerBuilder
    {
        $this->addOptions(CorsOptions::class, $config);
        return $this;
    }

    /**
     * Add HSTS Services.
     *
     * @param Closure $config(HstsOptions $options)
     * @return ContainerBuilder
     */
    public function addHsts(Closure $config = null): ContainerBuilder
    {
        $this->addOptions(HstsOptions::class, $config);
        return $this;
    }

    /**
     * Add HTTPS Redirection Services.
     *
     * @param Closure $config(HttpsRedirectionOptions $options)
     * @return ContainerBuilder
     */
    public function addHttpsRedirection(Closure $config = null): ContainerBuilder
    {
        $this->addOptions(HttpsRedirectionOptions::class, $config);
        return $this;
    }

    /**
     * Add Static Files Services.
     *
     * @param Closure $config(StaticFilesOptions $options)
     * @return ContainerBuilder
     */
    public function addStaticFiles(Closure $config = null): ContainerBuilder
    {
        $this->addOptions(StaticFilesOptions::class, $config);
        return $this;
    }

    /**
     * Add Plates Page Services.
     *
     * @param Closure|null $pages(PlatesPageOptions $pages)
     * @param Closure|null $pages(PlatesEngineOptions $engine)
     * @return ContainerBuilder
     */
    public function addPlatesPage(?Closure $pages = null, ?Closure $engine = null): ContainerBuilder
    {
        $this->addOptions(PlatesPageOptions::class, $pages);
        $this->addOptions(PlatesEngineOptions::class, $engine);
        return $this;
    }
}
