<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/web-application library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\WebApp\Internal;

use RuntimeException;
use Psr\Http\Message\ResponseInterface;

abstract class ResponseSender
{
    public function send(ResponseInterface $response): void
    {
        $this->assertNotSent();
        $this->sendStatusLine($response);
        $this->sendHeaders($response);
        $this->sendBody($response);
    }

    private function assertNotSent(): void
    {
        if (headers_sent())
        {
            throw new RuntimeException('Headers already sent');
        }
        while (ob_get_level() > 0)
        {
            if (!empty(ob_get_clean()))
            {
                throw new RuntimeException('Ouput already sent');
            }
        }
    }

    private function sendStatusLine(ResponseInterface $response): void
    {
        $statusLine = sprintf('HTTP/%s %s %s',
            $response->getProtocolVersion(),
            $response->getStatusCode(),
            $response->getReasonPhrase()
        );
        header($statusLine, true, $response->getStatusCode());
    }

    private function sendHeaders(ResponseInterface $response): void
    {
        foreach ($response->getHeaders() as $name => $values)
        {
            $name = ucwords($name, '-');
            foreach ($values as $value)
            {
                header("$name: $value", false);
            }
        }
        flush();
    }

    private function sendBody(ResponseInterface $response): void
    {
        $output = fopen('php://output', 'wb');

        $body = $response->getBody();
        if ($body->isSeekable())
        {
            $body->rewind();
        }
        $source = $body->detach();
        stream_copy_to_stream($source, $output);

        fclose($output);
    }
}
