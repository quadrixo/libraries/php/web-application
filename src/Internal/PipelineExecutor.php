<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/web-application library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\WebApp\Internal;

use Closure;
use Generator;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Quadrixo\ContainerAwareTrait;

/**
 * @internal Exceute a pipeline through its Generator representation
 */
class PipelineExecutor implements RequestHandlerInterface
{
    use ContainerAwareTrait;

    /** @var Generator */
    private $middlewares;

    /** @var callable which move forward the middlewares generator */
    private $delegate;

    /**
     * @todo Find a way to remove this constructor
     */
    public function __construct(Generator $middelwares)
    {
        $this->middlewares = $middelwares;
        $this->delegate = [ $this, 'rewind' ];
    }

    /**
     * @inheritDoc
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $middleware = $this->next();

        try
        {
            if (is_string($middleware) && $this->getContainer()->has($middleware))
            {
                $middleware = $this->getContainer()->get($middleware);
            }

            if ($middleware instanceof MiddlewareInterface)
            {
                $response = $middleware->process($request, $this);
            }
            else if ($middleware instanceof Closure)
            {
                $response = $middleware->call($this->getContainer(), $request, $this);
            }
            else if (is_callable($middleware))
            {
                $response = $middleware($request, $this);
            }
            else if (!($middleware instanceof MiddlewareInterface))
            {
                $type = get_debug_type($middleware);
                throw new InvalidArgumentException("The type '$type' is invalid for middleware");
            }

            // Case when callback is a factory
            if ($response instanceof MiddlewareInterface)
            {
                $response = $response->process($request, $this);
            }
            return $response;
        }
        finally
        {
            $this->delegate = [ $this, 'rewind' ];
        }
    }

    private function next()
    {
        ($this->delegate)();
        return $this->middlewares->current() ?? $this->middlewares->getReturn();
    }

    private function rewind()
    {
        $this->delegate = [ $this->middlewares, 'next' ];
        $this->middlewares->rewind();
    }
}
