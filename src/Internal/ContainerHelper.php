<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/web-application library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\WebApp\Internal;

use Closure;
use InvalidArgumentException;
use Psr\Container\ContainerInterface;
use ReflectionFunction;
use ReflectionNamedType;
use ReflectionParameter;

final class ContainerHelper
{
    private function __construct()
    {
    }

    /**
     * Resolve parameters with `$container` before calling the closure.
     *
     * @param ContainerInterface $container
     * @param Closure $closure
     * @return mixed
     */
    public static function Execute(ContainerInterface $container, Closure $closure)
    {
        $function = new ReflectionFunction($closure);
        $parameters = array_map(function(ReflectionParameter $p) use($container)
        {
            $type = $p->getType();
            if ($type instanceof ReflectionNamedType && !$type->isBuiltin())
            {
                return $container->get($type->getName());
            }
            throw new InvalidArgumentException("Parameter {$p->getName()} is not injectable");
        }, $function->getParameters());
        return $function->invokeArgs($parameters);
    }
}
