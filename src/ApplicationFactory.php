<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/web-application library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\WebApp;

use Nyholm\Psr7\Factory\Psr17Factory;
use Nyholm\Psr7Server\ServerRequestCreator;
use Nyholm\Psr7Server\ServerRequestCreatorInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ServerRequestFactoryInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UploadedFileFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use Quadrixo\Application;
use Quadrixo\ContainerAwareTrait;
use Quadrixo\ContainerBuilderInterface;
use Quadrixo\WebApp\Internal\ContainerHelper;

/**
 * The application factory class
 */
class ApplicationFactory
{
    /**
     * Create a new instance of ApplicationFactory with
     * content root directory as `$_SERVER['DOCUMENT_ROOT']`
     * and public web content as `'public'`.
     *
     * @return ApplicationFactory
     */
    public static function new(): ApplicationFactory
    {
        return new static();
    }

    private $container;
    private $appConfigure;

    /**
     * Configure with a ContainerBuilder the container to be used by the application.
     *
     * @param callable(ContainerBuilderInterface) $configure
     * @return static
     */
    public function configureContainer(callable $configure): ApplicationFactory
    {
        assert(is_null($this->container));
        $this->container = $configure;
        return $this;
    }

    /**
     * Configure the application.
     *
     * @param callable(Applicationbuilder) $app
     * @return static
     */
    public function configure(callable $configure): ApplicationFactory
    {
        $this->appConfigure = $configure;
        return $this;
    }

    /**
     * Create the handler corresponding to this application
     *
     * @return Application
     */
    public function create(): Application
    {
        $container = $this->getContainer();
        if (!is_null($this->appConfigure))
        {
            ContainerHelper::Execute($container, $this->appConfigure);
        }
        return $container->get(ApplicationBuilder::class)->build();
    }

    /**
     * Create and run the application
     *
     * @param ServerRequestInterface $request
     * @return void
     */
    public function run(ServerRequestInterface $request = null): void
    {
        $this->create()->run($request);
    }

    /**
     * Override this method to use a custom builder for container.
     *
     * @return ContainerBuilderInterface
     */
    protected function getContainerBuilder(): ContainerBuilderInterface
    {
        return new class() extends ContainerBuilder {};
    }

    /**
     * Override this method to use a custom container.
     *
     * @return ContainerInterface
     */
    protected function getContainer(): ContainerInterface
    {
        $builder = $this->getContainerBuilder()
            // Inject PS,7R-7 factories
            ->add(Psr17Factory::class, Psr17Factory::class, true)
            ->add(RequestFactoryInterface::class, Psr17Factory::class)
            ->add(ResponseFactoryInterface::class, Psr17Factory::class)
            ->add(ServerRequestFactoryInterface::class, Psr17Factory::class)
            ->add(StreamFactoryInterface::class, Psr17Factory::class)
            ->add(UploadedFileFactoryInterface::class, Psr17Factory::class)
            ->add(UriFactoryInterface::class, Psr17Factory::class)
            ->add(ServerRequestCreatorInterface::class, ServerRequestCreator::class)

            // Declare default ApplicationBuilder implementation
            ->add(ApplicationBuilder::class, function(ContainerInterface $container)
            {
                return (new class() extends ApplicationBuilder {})
                    ->setContainer($container);
            }, true);

        if (!is_null($this->container))
        {
            ($this->container)($builder);
        }
        $container = $builder->build();
        return $container;
    }
}
