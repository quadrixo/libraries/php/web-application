# Version 0.5.1 - 2021-09-10
## Added
- Add CORS extensions

# Version 0.5.0 - 2021-09-08
## Changed
- Adapt to the removing of `AppContext`

# Version 0.4.0 - 2021-09-06
## Changed
- This library is now a full-featured web application

# Version 0.3.0 - 2021-08-05
## Fixed
- Bad return value for the generator of middlewares

# Version 0.3.0 - 2021-08-05
## Changed
- Drop support for PHP < 7.3
- Prepare ApplicationFactory class to be overrided

# Version 0.2.0 - 2021-08-03
## Changed
- Add configuration function execution before middleware resolution

# Version 0.1.0 - 2021-03-30
## Added
- Release Candidate
